<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    const STATUS_KEY       = 'status';
    const STATUS_AVAILABLE = 'available';
    const STATUS_CHARGING  = 'charging';
    const STATUS_FULL      = 'full';
    protected $table = 'unit';
    protected $with  = ['tokens'];
    /**
     * @var UnitToken
     */
    protected $availableToken;
    /**
     * @var UnitToken
     */
    protected $usedToken;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|Charge[]
     */
    public function charges()
    {
        return
            $this->hasMany('App\Charge');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|UnitToken[]
     */
    public function tokens()
    {
        return
            $this->hasMany('App\UnitToken');
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return
            $this->getAttribute(self::STATUS_KEY);
    }

    /**
     * @param string $status
     * @return static
     */
    public function setStatus(string $status)
    {
        $this->setAttribute(self::STATUS_KEY, $status);

        return $this;
    }

    /**
     * @param string $user
     * @return string
     */
    public function personalizeStatus($user)
    {
        $this->setStatus(Unit::STATUS_FULL);

        /**
         * @var UnitToken $token
         */
        foreach ($this->tokens as $token) {
            if ($user === $token->used_by) {
                $this->setStatus(self::STATUS_CHARGING);
                $this->setUsedToken($token);
                break;
            }
            if (is_null($token->used_by)) {
                $this->setStatus(self::STATUS_AVAILABLE);
                $this->setAvailableToken($token);
            }
        }

        return
            $this->getStatus();
    }

    /**
     * @param string $user
     * @return Charge|bool
     */
    public function startCharge($user)
    {
        if (self::STATUS_AVAILABLE !== $this->personalizeStatus($user)) {
            return false;
        }

        $this->getAvailableToken()->setUserAndGenerateEventId($user);

        UnitToken::query()
            ->where('id', $this->getAvailableToken()->id)
            ->where('used_by', null)
            ->update([
                'used_by'  => $this->getAvailableToken()->used_by,
                'event_id' => $this->getAvailableToken()->event_id,
            ]);

        $reference = $this->getAvailableToken()->fresh();

        if ($reference->used_by !== $user) {
            return false;
        }

        $charge = new Charge();
        $charge
            ->setAttribute('unit_id', $this->id)
            ->setAttribute('unit_token_id', $this->getAvailableToken()->id)
            ->setAttribute('event_id', $this->getAvailableToken()->event_id)
            ->setAttribute('start', date("Y-m-d H:i:s", time()))
            ->setAttribute('user', $user);

        if (!$charge->save()) {
            $this->getAvailableToken()
                ->setAttribute('used_by', null)
                ->setAttribute('event_id', null)
                ->save();

            return false;
        }

        return $charge;
    }

    /**
     * @param string $user
     * @return bool
     */
    public function stopCharge($user)
    {
        if (self::STATUS_CHARGING !== $this->personalizeStatus($user)) {
            return false;
        }

        $charge =
            Charge::query()
                ->where('unit_token_id', $this->getUsedToken()->id)
                ->where('event_id', $this->getUsedToken()->event_id)
                ->first();

        $charge->setAttribute('end', date("Y-m-d H:i:s", time()));

        if (!$charge->save()) {
            return false;
        }

        return
            $this->getUsedToken()
                ->setAttribute('used_by', null)
                ->setAttribute('event_id', null)
                ->save();
    }

    /**
     * @return UnitToken
     */
    public function getAvailableToken(): UnitToken
    {
        return $this->availableToken;
    }

    /**
     * @param UnitToken $availableToken
     * @return static
     */
    public function setAvailableToken(UnitToken $availableToken)
    {
        $this->availableToken = $availableToken;

        return $this;
    }

    /**
     * @return UnitToken
     */
    public function getUsedToken(): UnitToken
    {
        return $this->usedToken;
    }

    /**
     * @param UnitToken $usedToken
     * @return static
     */
    public function setUsedToken(UnitToken $usedToken)
    {
        $this->usedToken = $usedToken;

        return $this;
    }
}
