<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnitToken extends Model
{
    protected $table = 'unit_token';

    /**
     * @param $user
     * @return self
     */
    public function setUserAndGenerateEventId($user)
    {
        $this->setAttribute('used_by', $user);

        return
            $this->generateEventId();
    }

    /**
     * @return self
     */
    public function generateEventId()
    {
        $this->setAttribute(
            'event_id',
            hash('sha512', time() . $this->used_by . rand())
        );

        return $this;
    }
}
