<?php

namespace App\Http\Controllers;

use App\Http\Requests\ChangeUserRequest;
use App\Unit;
use Illuminate\Database\Connection;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class PodPoint extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function user(ChangeUserRequest $request)
    {
        if ($request->input()) {
            try {
                $this->setUser(
                    Arr::get(
                        $request->validated(),
                        'user'
                    )
                );
            } catch (\Exception $e) {
                response()->make()->setStatusCode(400);
            }
        }

        return
            response()->json(
                $this->getUser()
            );
    }

    public function units()
    {

        $units =
            Unit::with(
                [
                    'charges' => function ($query) {
                        /**
                         * @var \Illuminate\Database\Eloquent\Builder $query
                         */
                        $query->where('user', $this->getUser());
                    },
                ]
            )
                ->get()->all();
        /**
         * @var Unit $unit
         */
        foreach ($units as $unit) {
            $unit->personalizeStatus($this->getUser());
        }

        return
            response()->json($units);
    }

    public function unitDetails($id)
    {
        $unit =
            Unit::with('charges')->firstWhere(
                'id',
                $id
            );

        if (!$unit) {
            return
                response()->make()->setStatusCode(404);
        }

        return
            response()->json($unit);
    }

    public function startCharge($id)
    {
        /**
         * @var Unit $unit
         */
        $unit = Unit::query()->firstWhere('id', $id);

        if (
            $unit
            && $charge =
                $unit->startCharge(
                    $this->getUser()
                )
        ) {
            return
                response()
                    ->json()
                    ->header('X-Entity-ID', $charge->id);
        }

        return
            response()->make()->setStatusCode(400);
    }

    public function stopCharge($id)
    {
        /**
         * @var Unit $unit
         */
        $unit = Unit::query()->firstWhere('id', $id);
        if (!$unit) {
            return
                response()->make()->setStatusCode(400);
        }
        if (!$unit->stopCharge(
            $this->getUser()
        )) {
            return
                response()->make()->setStatusCode(404);
        }

        return
            response()->make()->setStatusCode(200);
    }

    public function index()
    {
        if (!$this->getUser()) {
            $this->setDefaultUser();
        }

        return
            response()->view('index');
    }

    protected function setUser($user)
    {
        request()->session()->put('user', $user);
    }

    protected function setDefaultUser()
    {
        $this->setUser('user1');
    }

    /**
     * @return string
     */
    protected function getUser()
    {
        return
            request()->session()->get('user');
    }
}
