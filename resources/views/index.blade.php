<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" xmlns:v-on="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>PodPoint</title>

    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"
          integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"
            integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"
            integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.2/axios.min.js"></script>

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/icons.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/style.css') }}">
</head>
<body>

<div class="full-height" id="PodPoint">
    <i class="loader-icon fa fa-spin fa-plug"></i>
    <div class="title m-b-md header">
        <strong> @{{ pageTitle }} </strong>
    </div>
    <div class="content">
        <unit-item
                v-for="unit in units"
                v-bind:key="unit.id"
                v-bind:id="unit.id"
                v-bind:name="unit.name"
                v-bind:address="unit.address"
                v-bind:postcode="unit.postcode"
                v-bind:status="unit.status"
                v-bind:charges="unit.charges"
        />
    </div>
    <div class="col-xs-12 footer text-center">
        <div class="col-xs-4">
            <div class="row"><i class="fa fa-star-o fa-2x"></i></div>
            Favourites
        </div>
        <div class="col-xs-4" v-on:click="getUnits">
            <div class="row"><i class="fa fa-bolt fa-2x"></i></div>
            Units
        </div>
        <div class="col-xs-4" data-toggle="modal" data-target="#accountModal">
            <div class="row"><i class="fa fa-user-circle-o fa-2x"></i></div>
            Account
        </div>
    </div>
    <div class="modal fade" ref="accountModal" id="accountModal" tabindex="-1" role="dialog"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title text-center">Change account</h5>
                </div>
                <div class="modal-body">
                    <input type="text" class="form-control" placeholder="Account" aria-label="Account"
                           v-model="user" v-on:keyup.enter="changeUser"/>
                </div>
                <div class="modal-footer">
                    <div class=" text-center">
                        <button type="button" class="btn btn-success" v-on:click="changeUser">Update</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" ref="messageModal" id="messageModal" tabindex="-1" role="dialog"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-borderless">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h5 class="modal-title text-center"></h5>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/x-template" id="unitItemTemplate">
    <div class="unit-item col-xs-12">
        <div class="col-xs-12">
            <div class="row">
                <div class="col-xs-2">
                    <i class="fa fa-bolt fa-3x text-green"></i>
                </div>
                <div class="col-xs-7 text-left">
                    <div class="row break-all"><b>@{{ name }}</b></div>
                    <div class="row">@{{ address }} - @{{ postcode }}</div>
                </div>
                <div class="col-xs-3 text-center">
                    <div class="row text-uppercase">@{{ status }}</div>
                    <button
                            type="button" class="btn col-xs-12 text-capitalize"
                            v-bind:class="buttonClass" v-on:click="actOnUnit"
                    >
                        @{{ buttonLabel }}
                    </button>
                </div>
            </div>
            <div class="row text-center">
                @{{ chargesCountText }}
            </div>
        </div>
    </div>
</script>
<script src="{{ asset('assets/js/app.js') }}"></script>
</body>
</html>
