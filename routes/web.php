<?php

use Illuminate\Support\Facades\Route;

Route::get('/', "PodPoint@index");
Route::get('/units', "PodPoint@units");
Route::get('/units/{id}', "PodPoint@unitDetails")
    ->where([
        'id' => '[0-9]+',
    ]);
Route::post('/units/{id}', "PodPoint@startCharge")
    ->where([
        'id' => '[0-9]+',
    ]);
Route::patch('/units/{id}', "PodPoint@stopCharge")
    ->where([
        'id'       => '[0-9]+',
    ]);

//Extra route for mocked account testing
Route::any('/user', "PodPoint@user");
