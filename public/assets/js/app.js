Vue.component('unit-item', {
    template: '#unitItemTemplate',
    props   : {
        id      : null,
        name    : null,
        address : null,
        postcode: null,
        charges : null,
        status  : null,
    },
    computed: {
        buttonLabel() {

            switch (this.status) {
                case 'available':
                    return 'start';
                case 'charging':
                    return 'stop';
                default:
                    return 'wait';
            }
        },
        buttonClass() {
            switch (this.status) {
                case 'available':
                    return 'btn-success';
                case 'charging':
                    return 'btn-warning';
                default:
                    return 'btn-secondary disabled';
            }
        },
        chargesCountText() {
            let count = this.charges.length;
            if (0 === count) {
                return 'No charges yet';
            }
            if (count > 1) {
                return count.toString() + ' charges';
            }
            return '1 charge';
        }
    },
    methods : {
        message(text){
            $(PodPointApp.$refs.messageModal).find('.modal-title').text(text);
            $(PodPointApp.$refs.messageModal).modal();
        },
        actOnUnit() {
            switch (this.status) {
                case 'available':
                    axios.post(['/units', this.id].join('/'))
                        .then(function (response) {
                        })
                        .catch(function (error) {
                            PodPointApp.message('Charge not started');
                            console.log(error);
                        })
                        .then(function () {
                            PodPointApp.getUnits();
                        });
                    break;
                case 'charging':
                    axios.patch(['/units', this.id].join('/'))
                        .then(function (response) {
                        })
                        .catch(function (error) {
                            PodPointApp.message('Charge not stopped');
                            console.log(error);
                        })
                        .then(function () {
                            PodPointApp.getUnits();
                        });
                    break;
            }
        }
    }
});
let PodPointApp = new Vue({
    el     : '#PodPoint',
    data   : {
        pageTitle: 'Units',
        user     : null,
        units    : [],
    },
    async created() {
        await this.getUser();
        this.getUnits();
    },
    methods: {
        async getUser() {
            await axios.get('/user')
                .then(function (response) {
                    PodPointApp.user = response.data;
                })
                .catch(function (error) {
                    console.log(error);
                });
        },
        getUnits() {
            $('.loader-icon').addClass('active');
            axios.get('/units')
                .then(function (response) {
                    PodPointApp.units = response.data;
                })
                .catch(function (error) {
                    console.log(error);
                })
                .then(function () {
                    $('.loader-icon').removeClass('active');
                });
        },
        changeUser() {
            axios.post('/user', {
                user: this.user
            })
                .then(function (response) {
                })
                .catch(function (error) {
                    console.log(error);
                })
                .then(function () {
                    $(PodPointApp.$refs.accountModal).modal('hide');
                    PodPointApp.getUnits();
                });
        }
    }
})