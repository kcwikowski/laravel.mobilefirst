/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;


DROP TABLE IF EXISTS `sessions`;
CREATE TABLE IF NOT EXISTS `sessions` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `unit`;
CREATE TABLE IF NOT EXISTS `unit` (
  `id`         INT(11)      NOT NULL AUTO_INCREMENT,
  `name`       VARCHAR(255) NOT NULL,
  `address`    VARCHAR(255) NOT NULL,
  `postcode`   VARCHAR(255) NOT NULL,
  `created_at` TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `postcode` (`postcode`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

/*!40000 ALTER TABLE `unit`
  DISABLE KEYS */;
INSERT INTO `unit` (`id`, `name`, `address`, `postcode`) VALUES
  (1, 'TATSFIELD', '74  Essex Rd', 'TN16 9QQ'),
  (2, 'AIRD LEIMHE', '87  Asfordby Rd', 'HS3 9JJ'),
  (3, 'THORPE BAY', '31  Buckingham Rd', 'SS1 4HT'),
  (4, 'SWANAGE', '30  Golf Road', 'BH19 2LF'),
  (5, 'OGBOURNE ST ANDREW', '83  Stroud Rd', 'SN8 5ET'),
  (6, 'DIPTFORD', '68  Manor Close', 'TQ9 3BB'),
  (7, 'CADELEIGH', '117  Circle Way', 'EX16 2UF'),
  (8, 'GELLILYDAN', '50  Park Terrace', 'LL41 5NX'),
  (10, 'FULLETBY', '76  Sandyhill Rd', 'LN9 6LJ'),
  (11, 'BRAUNTON', '22  Abingdon Road', 'EX33 5FN');
/*!40000 ALTER TABLE `unit`
  ENABLE KEYS */;


DROP TABLE IF EXISTS `unit_token`;
CREATE TABLE IF NOT EXISTS `unit_token` (
  `id`         INT(11)   NOT NULL AUTO_INCREMENT,
  `unit_id`    INT(11)   NOT NULL,
  `used_by`    VARCHAR(255)       DEFAULT NULL,
  `event_id`   CHAR(128)          DEFAULT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `used_by` (`used_by`),
  KEY `unit_id_used_by` (`unit_id`, `used_by`),
  CONSTRAINT `FK_unit_token_unit` FOREIGN KEY (`unit_id`) REFERENCES `unit` (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

/*!40000 ALTER TABLE `unit_token`
  DISABLE KEYS */;
INSERT INTO `unit_token` (`id`, `unit_id`, `used_by`, `event_id`) VALUES
  (1, 1, NULL, NULL),
  (2, 1, NULL, NULL),
  (3, 1, NULL, NULL),
  (4, 2, NULL, NULL),
  (5, 2, NULL, NULL),
  (6, 3, NULL, NULL),
  (7, 3, NULL, NULL),
  (8, 3, NULL, NULL),
  (9, 3, NULL, NULL),
  (10, 4, NULL, NULL),
  (11, 5, NULL, NULL),
  (12, 6, NULL, NULL),
  (13, 6, NULL, NULL),
  (14, 7, NULL, NULL),
  (15, 7, NULL, NULL),
  (16, 7, NULL, NULL),
  (17, 8, NULL, NULL),
  (18, 10, NULL, NULL),
  (19, 11, NULL, NULL),
  (20, 8, NULL, NULL);
/*!40000 ALTER TABLE `unit_token`
  ENABLE KEYS */;


DROP TABLE IF EXISTS `charge`;
CREATE TABLE IF NOT EXISTS `charge` (
  `id`            INT(11)      NOT NULL AUTO_INCREMENT,
  `unit_id`       INT(11)      NOT NULL,
  `unit_token_id` INT(11)      NOT NULL,
  `start`         TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `end`           TIMESTAMP    NULL     DEFAULT NULL,
  `user`          VARCHAR(255) NOT NULL,
  `event_id`      CHAR(128)    NOT NULL,
  `created_at`    TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at`    TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user` (`user`),
  KEY `FK_charge_unit` (`unit_id`),
  CONSTRAINT `FK_charge_unit` FOREIGN KEY (`unit_id`) REFERENCES `unit` (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 44
  DEFAULT CHARSET = utf8;


/*!40101 SET SQL_MODE = IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS = IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
